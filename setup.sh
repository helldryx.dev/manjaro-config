#!/bin/bash

#sudo pacman-mirrors -f 0
sudo pacman -Syu --noconfirm --needed
sudo pacman -S ansible sshpass yay --noconfirm --needed
yay -S pikaur --noconfirm --needed --rebuild

ssh-keyscan -H localhost >> ~/.ssh/known_hosts
ansible-playbook \
    -i localhost, \
    --ask-pass \
    --ask-become-pass \
    --ask-vault-pass \
    --tags "desktop" \
    playbook/full.yml
